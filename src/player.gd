extends ActorBase


func _physics_process(_delta: float) -> void:
	# Get linear velocity
	self.linear_velocity = get_linear_velocity()

	# Move the player
	self.linear_velocity = move_and_slide(
		self.linear_velocity * self.speed
	)


func get_linear_velocity() -> Vector2:
	"""Get current Player's linear velocity by inputs handle

	Velocity will be normalized if needed

	:Returns:
		Updated linear velocity
	"""

	var output_velocity := Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	)

	if output_velocity.length() > 1.0:
		output_velocity = output_velocity.normalized()

	return output_velocity
