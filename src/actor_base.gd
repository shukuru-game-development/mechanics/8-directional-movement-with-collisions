"""Base class for all actors"""

class_name ActorBase
extends KinematicBody2D

#
# Exported properties

export var speed: int = 16

#
# Public properties

var linear_velocity: Vector2 = Vector2.ZERO

#
# Private properties
