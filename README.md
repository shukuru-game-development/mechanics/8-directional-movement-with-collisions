# 8-directional movement with collisions

This Godot project implements the 8-directional movement with collision mechanic

## Movement

Update player's linear velocity from input strength handling as the vector length will always be
a range between -1.0 and 1.0 except for diagonal moveement where it needs to be normalized.

## Collision layers

Setting up proper collisions layers are the most important thing to do. The layer and mask rules that
you will define put your world in coherence. Don't forget to set a collision mask to tiles from tilemap that
need it

## Best practices

A base class named `ActorBase` which is inherited by Player class
